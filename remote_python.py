#! /home/matthew/anaconda3/bin/python

import paramiko
import time
import json
import argparse
import configparser
import sys
import os

class Client:
    """
    Wraps the connection to the host to make sure it closes
    at the end
    """
    def __enter__(self):
        self.config = configparser.ConfigParser()
        self.config.read("remote_python.conf")
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(self.config["DEFAULT"]["hostname"], username=self.config["DEFAULT"]["username"], password=self.config["DEFAULT"]["password"])
        self.channel = self.client.get_transport().open_session()
        return self.channel
    def __exit__(self, type, value, traceback):
        self.client.close()

class PseudoTerm:
    def start(self, args=sys.argv[1:]):
        """
        Passes the arguments in sys.argv to the remote host
        """
        self.client.send(" ".join(args) + "\n")

    def receive(self):
        """This function receives output from the SSH Session."""
        while True:
            if self.client.recv_ready():
                    output = self.client.recv(1024)
                    print(output.decode(), end='')
            else:
                time.sleep(0.5)
                if not(self.client.recv_ready()):
                    break

    def send(self, command):
        """This function sends input to the SSH Session."""
        if command == "exit":
            exit()
        else:
            self.client.send(command + "\n")

    def interactive(self):
        """This function is for starting interacting with programs, such as python in
        interactive mode. It starts the program, and sends a line, then waits for a 
        response until an exit command is issued"""
        while True:
            self.send(input())
            self.receive()

    def __init__(self, args=sys.argv[1:]):
        with Client() as self.client:
            self.client.get_pty()
            self.client.invoke_shell()
            self.start(args)
            self.receive()
            self.interactive()



if __name__=="__main__":
    PseudoTerm()
