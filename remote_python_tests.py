import unittest
import paramiko
#import remote_python
import os

class RemotePythonTests(unittest.TestCase):
    def setUp(self):
        self.test_server = "mglover.us"
        self.test_username = "matthew"
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(self.test_server, username=self.test_username)
        self.channel = client.get_transport().open_session()
        self.channel.get_pty()
        self.channel.invoke_shell()

    def test_hostname(self):
        stdin, stdout, stderr = self.client.exec_command("""hostname""", get_pty=True)
        hostname = stdout.readlines()
        self.assertEqual(hostname[0].rstrip(), "mglover.us")
        print("\nhostname Output: {0}".format(hostname))

    def test_python(self):
        stdin, stdout, stderr = self.client.exec_command("""python""", get_pty=True)
        python_version = stdout.readlines()

if __name__=="__main__":
    unittest.main()

        
