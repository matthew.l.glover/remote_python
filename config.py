import configparser

config = configparser.ConfigParser()
config['DEFAULT'] = {"hostname": "host", "username": "user", "password": "pass"}
with open("remote_python.conf", "w") as configfile:
    config.write(configfile)
